package handlers

import (
	"strings"
	"time"
	"encoding/base64"
	"github.com/rs/xid"
	"encoding/json"
	"path/filepath"
	"html/template"
	"database/sql"
	"net/url"
	"net/http"
	"oauth2-projects/implicit-flow-examples/oauth-server/model"
)


func AuthFormHandler(w http.ResponseWriter, r *http.Request) {
	username := r.FormValue("username")
	password := r.FormValue("password")

	rawQuery := r.URL.RawQuery
	parseQuery , _ := url.ParseQuery(rawQuery)

	qResponseType := parseQuery.Get("response_type")
	qClientId := parseQuery.Get("client_id")
	qRedirectUri := parseQuery.Get("redirect_uri")
	qScope := parseQuery.Get("scope")

	row := models.DB.QueryRow("select * from user where username = ? and password = ?",username , password)
	err := row.Scan(&username,&password)
	if err == sql.ErrNoRows {
		w.Write([]byte("Invalid username or password"))
		return
	}

	s , _ := models.RediStore.Get(r , "implicit-session")
	s.Values["username"] = username
	s.Save(r , w)
	_ = "/oauth/auth?response_type="+qResponseType+"&client_id="+qClientId+"&redirect_uri="+qRedirectUri+"&scope="+qScope

	http.Redirect(w , r , r.URL.String() ,302)
}

func AuthHandler(w http.ResponseWriter, r *http.Request) {
	s , _ := models.RediStore.Get(r , "implicit-session")
	username , ok := s.Values["username"]
	
	rawQuery := r.URL.RawQuery
	parseQuery , _ := url.ParseQuery(rawQuery)

	qResponseType := parseQuery.Get("response_type")
	qClientId := parseQuery.Get("client_id")
	qRedirectUri := parseQuery.Get("redirect_uri")
	qScope := parseQuery.Get("scope")

	row := models.DB.QueryRow("select * from implicit_client where client_id = ?",qClientId)
	var clientId , redirectUris , tAMethod, grantTypes, responseTypes, clientName, scope string
	err := row.Scan(
		&clientId,
		&redirectUris,
		&tAMethod,
		&grantTypes,
		&responseTypes,
		&clientName,
		&scope,
	) 	

	if err == sql.ErrNoRows {
		w.Header().Set("Content-type","application/json")
		w.WriteHeader(500)
		errResp := models.NewApiError("server_error","Client not registered yet")
		b , _ := json.Marshal(errResp)
		w.Write(b)
		return
	}

	dataMap := map[string]interface{}{
		"isLogin":ok,
		"response_type":qResponseType,
		"redirect_uri":qRedirectUri,
		"client_id":qClientId,
		"client_name":clientName,
		"scope":strings.Split(qScope , " "),
	}

	if ok {
		// create new token here...
		newToken := xid.New().String()
		e64Token := base64.StdEncoding.EncodeToString([]byte(newToken))
		dataToken := map[string]interface{}{
			"access_token":e64Token,
			"username":username,
			"token_type":"bearer",
			"expires_in":3600,	
		}

		b , _ := json.Marshal(dataToken)
		_ , err = models.RediDb.Set("token:"+e64Token,b , time.Hour).Result()

		if err != nil {
			w.WriteHeader(500)
			w.Header().Set("Content-type","application/json")
			aErr := models.NewApiError("server_error","Cannot proceed access_token")
			b , _ := json.Marshal(aErr)
			w.Write(b)
			return
		}

		grantedUri := qRedirectUri+"#access_token="+e64Token+"&token_type=bearer&expires_in=3600"
		deniedUri := qRedirectUri+"#error=access_denied"
		
		dataMap["username"] = username
		dataMap["granted_uri"] = grantedUri
		dataMap["denied_uri"] = deniedUri
	}

	publicPath , _ := filepath.Abs("oauth-server/public")
	loginHtml := filepath.Join(publicPath , "login.html")
	t := template.Must(template.ParseFiles(loginHtml))
	t.Execute(w , dataMap)
}