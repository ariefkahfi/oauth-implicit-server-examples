package handlers 

import (
	"fmt"
	"encoding/json"
	"oauth2-projects/implicit-flow-examples/oauth-server/model"
	"net/http"
)


func TokenInfoHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type","application/json")
	w.Header().Set("Access-Control-Allow-Origin","*")

	
	aToken := r.FormValue("token")

	if aToken == "" {
		w.WriteHeader(http.StatusBadRequest)
		b , _ := json.Marshal(models.NewApiError(
			"invalid_request",
			"missing parameter token",
		))
		w.Write(b)
		return
	}

	ss , err := models.RediDb.Get("token:"+aToken).Result()
	if err != nil {
		b , _ := json.Marshal(map[string]interface{}{
			"active":false,
		})
		w.Write(b)
		return
	}

	var dataToken map[string]interface{}
	err = json.Unmarshal([]byte(ss),&dataToken)
	
	fmt.Println("ss...",ss)
	fmt.Println("dataToken...",dataToken)

	respMap := map[string]interface{}{
		"username" : dataToken["username"].(string),
		"active" : true,
	}
	b , _ := json.Marshal(respMap)
	w.Write(b)
}