package handlers

import (
	"strings"
	"path/filepath"
	"encoding/json"
	"oauth2-projects/implicit-flow-examples/oauth-server/model"
	"redis-projects/redis-webapp2/utils"
	"github.com/rs/xid"
	"io/ioutil"
	"net/http"
)


func FormRegisterHandler(w http.ResponseWriter, r *http.Request) {
	publicPath , _ := filepath.Abs("oauth-server/public")
	http.ServeFile(w , r , filepath.Join(publicPath , "register.html"))
}	

func RegisterHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type","application/json")

	rr , err := ioutil.ReadAll(r.Body)
	logger.CheckError(err)

	var reqBody models.ImplicitClient
	err = json.Unmarshal(rr , &reqBody)
	logger.CheckError(err)

	reqBody.ClientId = xid.New().String()
	reqBody.ResponseTypes = []string{
		"token",
	}

	sRedUris := strings.Join(reqBody.RedirectUris , ";")
	sGraTypes := strings.Join(reqBody.GrantTypes, ";")
	sRespTypes := strings.Join(reqBody.ResponseTypes,";")
	
	if _ , err = models.DB.Exec("insert into implicit_client values (?,?,?,?,?,?,?)",
		reqBody.ClientId,
		sRedUris,
		reqBody.TokenEndpointAuthMethod,
		sGraTypes,
		sRespTypes,
		reqBody.ClientName,
		reqBody.Scope,
	); err != nil{
		w.WriteHeader(500)
		apiResp := models.NewApiResponse(500 , "Cannot register this client")
		b , _ := json.Marshal(apiResp)
		w.Write(b)
		return
	}

	
	resp , err := json.Marshal(reqBody)
	logger.CheckError(err)
	w.Write(resp)
}