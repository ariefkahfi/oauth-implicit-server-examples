package models

import (
	"github.com/go-redis/redis"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"gopkg.in/boj/redistore.v1"
)

var (
	DB , ERR_DB = sql.Open("mysql","arief:arief@/golang_db")
	RediStore , ERR_REDIS_STORE = redistore.NewRediStore(10,"tcp",":6379","",[]byte("ASD"))
	RediDb = redis.NewClient(&redis.Options{
		Addr:":6379",
		DB:0,
	})
)

