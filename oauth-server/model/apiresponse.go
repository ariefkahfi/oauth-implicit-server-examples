package models

type ApiResponse struct {
	Code int `json:"code"`
	Data interface{} `json:"data"`
}


type ApiError struct {
	ErrorCode string `json:"error_code"`
	ErrorDescription string `json:"error_description"`
}

func NewApiError(errorCode, errorDescription string) ApiError {
	return ApiError{
		errorCode,
		errorDescription,
	}
}

func NewApiResponse(code  int , data interface{}) ApiResponse {
	return ApiResponse{
		code,data,
	}
}