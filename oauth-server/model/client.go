package models

type ImplicitClient struct {
	ClientId string `json:"client_id"`
	RedirectUris []string `json:"redirect_uris"`
	ClientName string `json:"client_name"`
	TokenEndpointAuthMethod string `json:"token_endpoint_auth_method"`
	GrantTypes []string `json:"grant_types"`
	ResponseTypes []string `json:"response_types"` 
	Scope string `json:"scope"`
}

func NewImplicitClient(
	clientId,
	clientName,
	scope,
	tokenEndpointAuthMethod string,
	redUris,
	respTypes,
	graTypes []string,
) ImplicitClient {
	return ImplicitClient{
		ClientId:clientId,
		Scope:scope,
		ClientName:clientName,
		TokenEndpointAuthMethod:tokenEndpointAuthMethod,
		RedirectUris:redUris,
		GrantTypes:graTypes,
	}
}