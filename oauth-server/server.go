package main

import (
	"oauth2-projects/implicit-flow-examples/oauth-server/handlers"
	"net/http"
	"github.com/gorilla/mux"
)

func main() {
	r := mux.NewRouter()	


	r.Path("/register").Methods("GET").
		HandlerFunc(handlers.FormRegisterHandler)

	r.Path("/register").Methods("POST").
		Headers("Content-type","application/json").
		HandlerFunc(handlers.RegisterHandler)

	r.Path("/oauth/auth").Methods("GET").
		HandlerFunc(handlers.AuthHandler)	

	r.Path("/oauth/auth").Methods("POST").
		HandlerFunc(handlers.AuthFormHandler)
	
	r.Path("/oauth/introspect").Methods("POST").
		HandlerFunc(handlers.TokenInfoHandler)
	


	http.ListenAndServe(":9090",r)
}