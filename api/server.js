const express = require('express')
const app = express()
const PORT = 9600
const cors = require('cors')
const request = require('request')
const Sequelize = require('sequelize')
const sequelize = new Sequelize('golang_db','arief','arief',{
    operatorsAliases:false,
    dialect:'mysql'
})

const UserModel = sequelize.define('user',{
    username:{
        type:Sequelize.STRING,
        primaryKey:true
    },
    password:{
        type:Sequelize.STRING,
    }
},{
    tableName:'user',
    timestamps:false
})

sequelize.sync()
app.use(cors())


function introspectHandler(req,res,next) {
    console.log('on introspectHandler...')
    const accessToken = req.header('Authorization').split(' ')[1]

    if(!accessToken) {
        res.status(401).json({
            code:401,
            data:'Invalid token'
        })
        return
    }
    request
        .post('http://localhost:9090/oauth/introspect',{
            form:{
                token:accessToken
            }
        },(err,httpResponse , body)=>{

            if(err || !body){
                res.status(500).json({
                    code:500,
                    data:'ERR_INTERNAL_SERVER'
                })
                return
            }

            const parsedBody = JSON.parse(body)

            if(!parsedBody.active) {
                res.status(401).json({
                    code:401,
                    data:"Invalid token"
                })
                return
            }
            next()
        })
}

app.get('/user/:name',introspectHandler,(req,res)=>{
    UserModel.findByPk(req.params.name)
        .then(r=>{
            res.json({
                code:200,
                data:r
            })
        })
        .catch(err=>{
            res.status(500).json({
                code:500,
                data:err.message
            })
        })
})


app.listen(PORT , ()=> console.log(`listening on ${PORT}`))

